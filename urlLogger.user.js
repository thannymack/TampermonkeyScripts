// ==UserScript==
// @name         URL Logger
// @author       Thanael
// @version      0.3
// @description  Log URLs.
// @match        http://*/*
// @include      *
// @connect      insight.rapid7.com
// @connect      localhost
// @connect      127.0.0.1
// @connect      self
// @connect      10.3.20.217
// @connect      *
// @grant        GM.xmlHttpRequest
// @grant        unsafeWindow
// @grant        GM_setClipboard
// @run-at       document-end
// ==/UserScript==
//
//*************************************************************************
// Usage:
//
//    urlLog.toggle(); to turn on or off.
//    urlLog.list();   to list URLs found so far.
//*************************************************************************
//
/*--------------------------------------------------------------------------------------------------------------------
 -----------------------------------------------------Script Structure------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------
*/
(function() {
    'use strict';
    // Helper function to create new DOM elements, by thanael - thannymack.com
    function addEl() {
        function isElement(element) { // Check if something is a DOM element
            // This function from https://stackoverflow.com/a/36894871
            return element instanceof Element || element instanceof HTMLDocument;
        }
        // Defaults, if the user specifies nothing:
        let el = document.body; // add our new element to the document.body by default
        let tag = "div"; // add a DIV element by default
        let attr = {}; // no attributes by default
        for (let i = 0, j = arguments.length; i < j; i++) { // for each argument the user supplied
            // Idea from https://stackoverflow.com/a/51019068
            if (isElement(arguments[i])) { // if it's a DOM element
                el = arguments[i]; // assign it to the el variable
            }
            else if (typeof arguments[i] === "object") { // if it's a normal object
                attr = arguments[i]; // assign it to the attributes variable
            }
            else if (typeof arguments[i] === "string") { // or if it's a string
                tag = arguments[i]; // assign it to the tag name variable
            }
            else {
                return "You must specify a tag name (string) or a DOM element, or an object full of attribute names & values";
            }
        }
        const potentialHeadTags = ["style", "title", "link", "meta", "script", "base"]; // hardcoded list of potential tags that belong in <head>
        if (potentialHeadTags.includes(tag.toLowerCase()) && el === document.body) {
            // If the user has specified a tag usually put in the head,
            // and haven't supplied any other element:
            el = document.head; // we'll be appending our new element in the document.head
        }
        var insertBefore = false; // the user wants to insert the element before the element they specified in "el". Off by default.
        var append = true;
        var newEl = document.createElement(tag); // create the new element
        var attrKeys = Object.keys(attr); // generate an array of all attribute names the user supplied
        for (let i = 0, j = attrKeys.length; i < j; i++) { // for each attribute
            if (attrKeys[i] === "insertBefore") { // Checks for the presence of the "insertBefore" directive
                insertBefore = Boolean(attr[attrKeys[i]]); // convert to a boolean and assign
            }
            else if (attrKeys[i] === "append") { // Checks for the presence of the "append" directive
                append = Boolean(attr[attrKeys[i]]); // convert to a boolean and assign
            }
            else if (attrKeys[i].toLowerCase().startsWith("on") && typeof (attr[attrKeys[i]]) === "function") { // if the user is trying to add an event handler
                let handler = attrKeys[i].match(/on(.*)/i)[1]; // Regex out the actual event handler
                newEl.addEventListener(handler, attr[attrKeys[i]]);
            }
            else if (attrKeys[i] in el && !attrKeys[i].toLowerCase().startsWith("on")) {
                // if the user wants to specify a dot notation property (textContent etc)
                // "in" syntax from https://dmitripavlutin.com/check-if-object-has-property-javascript/
                // added the check for !startsWith("on") to handle edge cases where the user wants to supply a string containing JS
                // otherwise javascript will do el.onmouseover = "alert()" which achieves nothing.
                newEl[attrKeys[i]] = attr[attrKeys[i]]; // set the related element property to be whatever they asked for
            }
            else { // otherwise, they've just specified a regular attribute
                newEl.setAttribute(attrKeys[i], attr[attrKeys[i]]); // set it and forget
            }
        }
        if (!append){return newEl} // The user wants to append the element later. Return it to them as-is
        if (insertBefore) { // If the user assigned this as true, this is where we insert the element before the element they specified
            el.parentNode.insertBefore(newEl, el);
        }
        else { // Otherwise, we just append the element to the page
            el.appendChild(newEl)
        }
        return newEl;
    }
    try {
        var isEnabled = false; // default to not enabled, so we're not logging urls for every site ever.
        var urlLog = {}; // populate this with functions and information later down the line.
        urlLog.toggle = function(){ // switches on/off the urlLogger
            if (localStorage.url_logger_enabled){
                var checkIfEnabled = localStorage.url_logger_enabled;
                //  console.log(checkIfEnabled);
                if (checkIfEnabled == "false"){
                    localStorage.url_logger_enabled = true;
                    console.log("URL logging is now ON");
                }
                else if (checkIfEnabled == "true"){
                    localStorage.url_logger_enabled = false;
                    console.log("URL logging is now OFF");
                }
            }
            else{
                localStorage.url_logger_enabled = true;
                console.log("URL logging is now ON");
            }
        }
        urlLog.list = function (){ // lists URLs gathered
            var list = JSON.parse(localStorage.url_logger);
            console.log(list);
            var flatList = ""
            for (var i=0,j = list.length;i<j;i++){
                flatList = flatList + list[i] + "\n";
            }
            console.log(flatList.trim());
        }
        init(); // procedural function - kicks off everything needed, one at a time
        function init(){
            unsafeWindow.urlLog = urlLog; // sets the urlLog variable in the outer window, so the user can access & toggle the function
            checkIfEnabled(); // should the urlLogger run on this page?
            if (!isEnabled){return};
            initLocalStorage();
            logCurrentUrl();
            interceptBackgroundResponses();
            switchOnFetchIntercept();
        }

        function checkIfEnabled(){
            if (localStorage.url_logger_enabled){
                var checkIfEnabled = localStorage.url_logger_enabled;
                if (checkIfEnabled == "false"){isEnabled = false;}
                else if (checkIfEnabled == "true"){
                    isEnabled = true;
                }
            }
        }

        function logUrl(url){
            var list = JSON.parse(localStorage.url_logger);
            if (!list.includes(url)){
                list.push(url);
                localStorage.url_logger = JSON.stringify(list);
            }
        }
        function initLocalStorage(){
            if (!localStorage.url_logger){localStorage.url_logger = JSON.stringify([]);};
            if (!localStorage.url_logger_enabled){localStorage.url_logger_enabled = false;}
        }
        function logCurrentUrl(){
            var currentUrl = document.URL;
            logUrl(currentUrl);
        }
        function switchOnFetchIntercept(){
            const fetchIntercept = window.fetch;
            unsafeWindow.fetch = function() {
                // Get the parameter in arguments
                // Intercept the parameter here
                console.log(this);
                console.log(arguments);
                var url = findUrl(arguments);
                if (url){
                    logUrl(url);
                }
                //  alert(`fetch!!\n\n${url}`);
                addEl("div", document.body.firstChild, {textContent:`Fetch: ${url}`, insertBefore:true, style:"color:red;background-color:white;"});
                return fetchIntercept.apply(this, arguments);
                function findUrl(obj){
                    if (obj[0]){
                        if (typeof(obj[0]) === "string"){
                            if (obj[0].includes("http")){
                                return obj[0];
                            }
                            else {
                                return location.origin + obj[0];
                                return false;}
                        }
                        else if (typeof(obj[0]) === "object"){
                            if (obj[0].url){
                                return obj[0].url;
                            }
                            else {return false;}
                        }
                        else {return false;}
                    }
                    else{
                        console.log("Why does this array not have a zero?");
                        return false;
                    }
                }
            }
        }
        // intercept all background XHR requests so we can log them
        function interceptBackgroundResponses(){
            interceptAjax(processAjax); // run our global function to keep an eye on background XHR requests
            function processAjax(response,data){ // what will we do with the ajax?
                // console.log(response);
                var currentUrl = response.__zone_symbol__xhrURL || response.responseURL;
                if (!currentUrl.includes("http")){
                    currentUrl = unsafeWindow.location.origin + currentUrl;
                }
                logUrl(currentUrl);
            }
        }
        // intercept ajax requests sent to and from the page.
        function interceptAjax(callback,edit){
            let oldXHROpen = window.XMLHttpRequest.prototype.open; // redefine a normal XHR open function
            let oldXHRSend = window.XMLHttpRequest.prototype.send; // redefine a normal XHR send function
            window.XMLHttpRequest.prototype.open = function(method, url, async, user, password) {
                return oldXHROpen.apply(this, arguments); // then do the old XHR as IAS expects
            }
            window.XMLHttpRequest.prototype.send = function(data) {
                // do something with the method, url and etc.
                this.addEventListener('load', function() { // intercept the request
                    // do something with the response text
                    callback(this,data); // run the callback function, passing the response & post data
                });
                return oldXHRSend.apply(this, arguments);
            }
        }
    }
    catch(err){console.log(err);}
})();
//
//
/*----------------------------------------End of script--------------------------------------------*/
//
//